import { useState } from "react";
import styles from "./ContentToggler.module.css";

export default function ContentToggler(props) {
    const [visible, setVisible] = useState(!props.visible);
    const toggleContent = () => {
        setVisible(!visible);
    }
    return<>
        <div className={styles.presentation} onClick={toggleContent}>
        <svg  className={visible ? styles.inverser : 'styles.presentation'} width="24" height="24" xmlns="http://www.w3.org/2000/svg" fillRule="evenodd" clipRule="evenodd"><path d="M12 0c6.623 0 12 5.377 12 12s-5.377 12-12 12-12-5.377-12-12 5.377-12 12-12zm0 1c6.071 0 11 4.929 11 11s-4.929 11-11 11-11-4.929-11-11 4.929-11 11-11zm5.247 15l-5.247-6.44-5.263 6.44-.737-.678 6-7.322 6 7.335-.753.665z"/></svg>
        {props.title}
        </div>
        {!visible &&
        <div>
            {props.children}
        </div>
        }
    </>
}