import styles from './MenuNav.module.css';
export default function MenuNav(props) {
    return <nav className={styles.grand}>
        <ul className={styles.list}>
            <li className={styles.pages}>
                <button onClick={props.changePage('accueil')}>Accueil</button>
            </li>
            <li className={styles.pages}>
                <button onClick={props.changePage('projet1')} >Projet 1</button>
            </li>
            <li className={styles.pages}>
                <button onClick={props.changePage('projet2')} >Projet 2</button>
            </li>
        </ul>
    </nav>
}